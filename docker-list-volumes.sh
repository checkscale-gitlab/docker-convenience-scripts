#!/bin/bash
#
# Author: Christian Hübschi <christian@huebschi.ch>
#

DOCKER_VOLUMES=$(docker volume ls -q)

for VOLUME_ID in $(docker volume ls -q)
do

    if [ "$VOLUME_ID" != "DOCKER_VOLUMES" ]
    then

        DOCKER_VOLUME_SIZE=$(docker run --rm -t -v ${VOLUME_ID}:/volume_data alpine sh -c "du -hs /volume_data | cut -f1" )

        NUM_RELATED_CONTAINERS=$(docker ps -a --filter=volume=${VOLUME_ID} -q | wc -l)


        echo "Volume: ${VOLUME_ID}"
        echo "   Size: ${DOCKER_VOLUME_SIZE}" 
        echo "   Containers: ${NUM_RELATED_CONTAINERS}"
        
        echo

    fi
done
