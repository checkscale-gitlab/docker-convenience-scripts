#!/bin/bash
#
# Author: Christian Hübschi <christian@huebschi.ch>
#

# Example: /usr/local/bin
INSTALL_PATH='/usr/local/bin'

if [ "$1" = "" ]
then
        echo "Please provide a action [ install | update | remove ]"
        exit
fi

if [ "$1" = "install" ] 
then

    # https://docs.linuxserver.io/general/docker-compose
    # https://github.com/linuxserver/docker-docker-compose
    sudo curl -L --fail https://raw.githubusercontent.com/linuxserver/docker-docker-compose/master/run.sh -o ${INSTALL_PATH}/docker-compose
    sudo chmod +x ${INSTALL_PATH}/docker-compose

    exit
fi


if [ "$1" = "update" ] 
then

    # https://docs.linuxserver.io/general/docker-compose
    # https://github.com/linuxserver/docker-docker-compose
    docker pull linuxserver/docker-compose:"${DOCKER_COMPOSE_IMAGE_TAG:-latest}"
    docker image prune -f

    exit
fi


if [ "$1" = "remove" ] 
then

    sudo rm -rf ${INSTALL_PATH}/docker-compose

    exit
fi