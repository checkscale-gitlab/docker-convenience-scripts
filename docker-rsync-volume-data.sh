#!/bin/bash
#
# Author: Christian Hübschi <christian@huebschi.ch>
#

if [ "$1" = "" ]
then
        echo "Please provide a source volume name"
        exit
fi

if [ "$2" = "" ] 
then
    echo "Please provide a destination volume name"
    exit
fi

docker volume inspect $1 > /dev/null 2>&1
if [ "$?" != "0" ]
then
    echo "The source volume \"$1\" does not exist"
    exit
fi


docker volume inspect $2 > /dev/null 2>&1
if [ "$?" != "0" ]
then
        echo "The destination volume \"$1\" does not exist"
        exit
fi

echo "Copying data from source volume \"$1\" to destination volume \"$2\"..."
docker run --rm \
           -i \
           -t \
           -v $1:/source \
           -v $2:/destination \
           alpine ash -c "apk update \
           && apk upgrade \
           && apk add --no-cache rsync \
           && rsync -aHP --stats /source/ /destination/"