#!/bin/bash
#
# Author: Christian Hübschi <christian@huebschi.ch>
#

# :: environments

VOLUME_NAME=$1
NFS_TYPE='nfs4'
NFS_HOST_ADDR=$2
NFS_OPTIONS='rw,noatime,proto=tcp,sec=sys,rsize=8192,wsize=8192,timeo=600,hard'
NFS_HOST_SHARE=$3

# :: validate and check input

if [ "${VOLUME_NAME}" = "" ]
then
        echo "Please provide a volume name"
        exit
fi

docker volume inspect ${VOLUME_NAME} > /dev/null 2>&1
if [ "$?" = "0" ]
then
    echo "The volume \"${VOLUME_NAME}\" does already exist"
    exit
fi

if [ "${NFS_HOST_ADDR}" = "" ] 
then
    echo "Please provide a host address"
    exit
fi

if [ "${NFS_HOST_SHARE}" = "" ] 
then
    echo "Please provide a nfs host share"
    exit
fi

# :: create nfs volume

VOLUME_ID=$(docker volume create --driver local \
      --opt type=${NFS_TYPE} \
      --opt o=addr=${NFS_HOST_ADDR},${NFS_OPTIONS} \
      --opt device=:${NFS_HOST_SHARE} \
      ${VOLUME_NAME})

# :: try to mount the volume

DOCKER_VOLUME_SIZE=$(docker run --rm -t -v ${VOLUME_ID}:/volume_data alpine sh -c "du -hs /volume_data | cut -f1" )
DOCKER_VOLUME_DIRLIST=$(docker run --rm -t -v ${VOLUME_ID}:/volume_data alpine sh -c "ls -ahl /volume_data | cut -f1" )

# :: done

echo "Volume \"${VOLUME_ID}\" successfull created."
echo "   Volume size: ${DOCKER_VOLUME_SIZE}"
echo "   Volume data: ${DOCKER_VOLUME_DIRLIST}"